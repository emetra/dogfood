SET NOCOUNT ON;
EXEC AddSchema 'Dogfood';
GO

PRINT ' -- Sikrer at alle medlemmer i arbeidsgruppen har QuickStat-rollen';
ALTER ROLE QuickStat ADD MEMBER mrk;
ALTER ROLE QuickStat ADD MEMBER moe;
ALTER ROLE QuickStat ADD MEMBER tty;
ALTER ROLE QuickStat ADD MEMBER bni;
ALTER ROLE QuickStat ADD MEMBER een;
GO
GRANT EXECUTE ON SCHEMA::Report TO QuickStat AS dbo;
GRANT EXECUTE ON SCHEMA::Dogfood TO public as dbo;
GO

PRINT ' -- Sletter alle prosedyrer som hører til Dogfood-protokollen';
DELETE FROM DbProcList WHERE StudyName = 'Dogfood';
GO

PRINT ' -- Gjenoppretter populasjoner for denne protokollen.';
GO
DROP PROCEDURE Dogfood.GetCaseListDatabaseVersion;
GO
CREATE PROCEDURE Dogfood.GetCaseListDatabaseVersion( @StudyId INT ) AS
BEGIN
  SET LANGUAGE Norwegian;
  SELECT v.*, CONCAT( 'Versjon ',CONVERT(VARCHAR,CONVERT(INT,db.Quantity)), ' (', CONVERT(VARCHAR, db.EventTime, 106  ), ')' ) AS InfoText
  FROM dbo.GetLastQuantityTable( 3812, NULL ) db
  JOIN dbo.ViewActiveCaseListStub v ON v.PersonId = db.PersonId
  WHERE StudyId = @StudyId ORDER BY db.Quantity DESC, db.EventTime DESC;
END;
GO

INSERT INTO DbProcList ( ProcId, ListId, ProcName, ProcDesc, StudyName, ProcParams )
VALUES( 20001, 'CASE', 'Dogfood.GetCaseListDatabaseVersion', 'Liste over databaseversjoner', 'Dogfood', ':StudyId' );
GO

PRINT ' -- Populasjon som viser status på innsamling av data til NDV'
DROP PROCEDURE Dogfood.GetCaseListNdvDataCollected
GO
CREATE PROCEDURE Dogfood.GetCaseListNdvDataCollected( @StudyId INT ) AS
BEGIN
  SELECT PersonId, DOB, FullName, GroupName, IIF(LastCollected IS NULL,'Aldri hentet NDV-data','Siste datainnsamling: ' + CAST(Year(LastCollected) AS VARCHAR)) AS InfoText
  FROM (
    SELECT v.PersonId,v.DOB,v.FullName,v.GroupName, MAX(IIF(cdp.ItemId IS NOT NULL, ce.EventTime, NULL)) as LastCollected
    FROM dbo.ViewActiveCaseListStub v
    JOIN dbo.GetLastEnumValuesTable( 1496, NULL ) ndv ON ndv.PersonId = v.PersonId AND ndv.EnumVal = 4
    LEFT JOIN dbo.ClinEvent ce ON ce.PersonId = v.PersonId
    LEFT JOIN  dbo.ClinDataPoint cdp ON cdp.EventId = ce.EventId AND cdp.ItemId IN (4346,10547) AND cdp.EnumVal = 1
    WHERE v.StudyId = @StudyId 
    GROUP BY v.PersonId,v.DOB,v.FullName,v.GroupName) v
END;
GO

INSERT INTO DbProcList ( ProcId, ListId, ProcName, ProcDesc, StudyName, ProcParams )
VALUES( 20002, 'CASE', 'Dogfood.GetCaseListNdvDataCollected', 'Datainnsamling til NDV', '*', ':StudyId' );
GO

DROP PROCEDURE Dogfood.GetCaseListSoftwareVersion;
GO
CREATE PROCEDURE Dogfood.GetCaseListSoftwareVersion( @StudyId INT ) AS
BEGIN
  SET LANGUAGE Norwegian;
  SELECT v.*, CONCAT( 'Versjon ', db.TextVal ) AS InfoText
  FROM dbo.GetLastTextValuesTable( 3774, NULL ) db
  JOIN dbo.ViewActiveCaseListStub v ON v.PersonId = db.PersonId
  WHERE StudyId = @StudyId ORDER BY db.TextVal DESC;
END;
GO

INSERT INTO DbProcList ( ProcId, ListId, ProcName, ProcDesc, StudyName, ProcParams )
VALUES( 20003, 'CASE', 'Dogfood.GetCaseListSoftwareVersion', 'Liste over programversjoner', 'Dogfood', ':StudyId' );
GO

select * from dbo.GetLastTextValuesTable( 3774, NULL );

PRINT ' -- Populasjoner for RF';
DROP  PROCEDURE [Dogfood].[GetCaseListHelseForetak] ;

GO
CREATE PROCEDURE [Dogfood].[GetCaseListHelseForetak] (@StudyId INT, @RF INT) AS
BEGIN
	SET LANGUAGE Norwegian;
	SELECT a.PersonId, a.FullName, a.GroupName, a.InfoText
	FROM (SELECT v.PersonId, v.FullName, v.GroupName, mia2.OptionText AS InfoText, ROW_NUMBER() OVER (PARTITION BY v.PersonId ORDER BY ce.EventNum DESC) rnk
		FROM ViewActiveCaseListStub v
		JOIN dbo.ClinEvent ce
			ON v.PersonId = ce.PersonId AND v.StudyId = ce.StudyId
		JOIN dbo.ClinDataPoint cdp1
			ON ce.EventId = cdp1.EventId AND cdp1.ItemId = 3987
		JOIN dbo.ClinDataPoint cdp2
			ON cdp2.EventId = ce.EventId AND cdp2.ItemId = (3987 + cdp1.EnumVal)
		JOIN dbo.MetaItemAnswer mia1
			ON cdp1.ItemId = mia1.ItemId AND cdp1.EnumVal = mia1.OrderNumber
		JOIN dbo.MetaItemAnswer mia2
			ON cdp2.ItemId = mia2.ItemId AND cdp2.EnumVal = mia2.OrderNumber
		WHERE v.StudyId = @StudyId
		AND cdp1.EnumVal = @RF) a
	WHERE a.rnk = 1
END;

GO
INSERT INTO  dbo.DbProcList VALUES (20004, 'CASE','Dogfood.GetCaseListHelseForetak','RF: Helse Vest',':StudyId,1','*',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL);
INSERT INTO  dbo.DbProcList VALUES (20005, 'CASE','Dogfood.GetCaseListHelseForetak','RF: Helse Nord',':StudyId,2','*',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL);
INSERT INTO  dbo.DbProcList VALUES (20006, 'CASE','Dogfood.GetCaseListHelseForetak','RF: Helse Sør-Øst',':StudyId,3','*',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL);
INSERT INTO  dbo.DbProcList VALUES (20007, 'CASE','Dogfood.GetCaseListHelseForetak','RF: Helse Midt',':StudyId,4','*',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL);
GO



