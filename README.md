# Dogfood

## Formål

Formålet med denne protokollen er at alle som arbeider med **FastTrak** skal bli bedre kjent med systemet.
Dogfooding er forklart på Wikipedia.

[Eating your own dog food](https://en.wikipedia.org/wiki/Eating_your_own_dog_food "Dogfooding på Wikipedia")

Ved at alle i teamet bruker FastTrak selv tror vi at vi både kan oppdage feil og se nye muligheter.

## Retninglinjer

Retninglinjene er foreløpige, og vi diskuterer og/eller supplerer disse ved behov eller uenighet.

### Skjema

Skjema lages på samme måte som i alle andre protokoller.  Vi skal prøve å utnytte mest mulig av funksjonalitetene som finnes i skjemahåndteringen, slik som formler, skjulte variable, meldinger etc., slik at vår egen bruk blir en mest mulig omfattende testing.

### Populasjoner

Vi legger ikke inn populasjoner som vi lager her inn på metadataserveren, men lager dem via script.  Se i undermappe **SQL**.

### Stjele - ikke dele

Vi kan stjele ting fra andre protokoller (eks. `Nyheter.fr3 `fra GBD), men i utgangspunktet skal vi ikke dele rapporter osv. med andre protokoller med mindre det er veldig gode grunner til det. Også variable og skjema bør i hovedsak være unike for denne protokollen.  

